"""Tweeter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from tweet.views import new_tweet, tweets, reported_tweets, get_by_hashtag
from login.views import register

urlpatterns = [
    path('admin/', admin.site.urls),

    # tweet app
    path('new-tweet', new_tweet),
    path('', tweets),
    path('reported', reported_tweets),
    path('tweet/get/', get_by_hashtag),

    # login app
    path('register', register)
]

