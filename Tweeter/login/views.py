from django.shortcuts import render
from django.http import HttpResponse
from .models import UserTable
from .validators import username_is_ok, password_is_ok, name_is_ok, email_is_ok

# Create your views here.


def register(request):
    username = request.GET.get('username', None)
    password = request.GET.get('password', None)
    name = request.GET.get('name', None)
    email = request.GET.get('email', None)
    if not (username and password):
        return HttpResponse('Username or password is not provided')

    if not username_is_ok(username):
        return HttpResponse('Bad Username', status=400)

    try:
        user = UserTable.objects.get(username=username)
        return HttpResponse('Already Rgistered')
    except:
        pass

    if password_is_ok(password) and name_is_ok(name) and email_is_ok(email):
        # save
        user = UserTable()
        user.username = username
        user.password = password
        user.name = name
        user.email = email
        user.save()
        return HttpResponse("DONE", status=201)
    else:
        return HttpResponse("Bad Password or name or email")


def login(request):
    pass


