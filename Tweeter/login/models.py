from django.db import models

# Create your models here.


class UserTable(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    username = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    picture = models.ImageField(null=True)
    email = models.EmailField(blank=True, null=True, unique=True)


