from django.db import models
from django.conf import settings


class UserTweet(models.Model):
    # controlling options
    added_on = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    # data
    text = models.CharField(max_length=140)
    like = models.PositiveIntegerField(default=0)
    dislike = models.PositiveIntegerField(default=0)
    report = models.PositiveIntegerField(default=0)


