from django.shortcuts import render
from django.http import HttpResponse
from.models import UserTweet
from .validator import check_tweet

def new_tweet(request):
    tweet_text = request.GET.get('text', None)
    if not tweet_text:
        return HttpResponse('provide text', status=503)
    else:
        if check_tweet(tweet_text):
            # save to db
            tweet = UserTweet()
            tweet.text = tweet_text
            tweet.save()
            return HttpResponse(tweet_text, status=201)
        else:
            return HttpResponse(status=400)

def like_tweet(request):
    user_pk = request.GET.get('pk', None)
    if not user_pk:
        return HttpResponse('provide pk', status=400)
    else:
        tweet = UserTweet.objects.get(pk=user_pk)
        tweet.like += 1
        tweet.save()
        return HttpResponse(status=200)


def tweets(request):
    tweets = UserTweet.objects.all()
    res = []
    for tweet in tweets:
        res.append(
            {
                'TEXT': tweet.text,
                'LIKE': tweet.like
            }
        )
    return HttpResponse(res)



def reported_tweets(request):
    reported = UserTweet.objects.filter(report__gt=50)
    reported = reported.exclude(like__gte=100)
    res = []
    for tweet in reported:
        res.append(
            {
                'TEXT': tweet.text,
                'LIKE': tweet.like,
                'REPORTS': tweet.report
            }
        )
    return HttpResponse(res)

def get_by_hashtag(request):
    hashtag = request.GET.get('hashtag', None)

    if not hashtag:
        return HttpResponse('provide hashtag', status=400)

    tweets = UserTweet.objects.filter(text__contains=hashtag)
    res = []
    for tweet in tweets:
        res.append(
            {
                'TEXT': tweet.text,
                'LIKE': tweet.like,
                'REPORTS': tweet.report
            }
        )
    return HttpResponse(res, status=200)

