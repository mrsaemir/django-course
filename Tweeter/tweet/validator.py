BLACKLIST = ['hello']

def check_tweet(tweet):
    tweet = str(tweet)
    for word in tweet.split(" "):
        if word in BLACKLIST:
            return False
    return True